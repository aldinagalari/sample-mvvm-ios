//
//  ViewController.swift
//  MVVM
//
//  Created by Aldina Galari on 26/12/22.
//

import UIKit

class ViewController: UIViewController {

    internal lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 1000
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(CardUserCell.self,
                           forCellReuseIdentifier: CardUserCell.identifier())
        tableView.separatorColor = .clear
        tableView.showsVerticalScrollIndicator = false
        tableView.bounces = false
        tableView.backgroundColor = .lightGray.withAlphaComponent(0.2)
        tableView.reloadData()
        return tableView
    }()
    
    var viewModel = ViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Our Nakama"
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.viewModel.getUsers()
        self.addView()
        self.bindData()
    }

    fileprivate func bindData() {
        viewModel.getUserSuccess.bind { [weak self] _ in
            guard let sSelf = self else { return }
            DispatchQueue.main.async {
                sSelf.tableView.reloadData()
            }
        }
    }
}

extension ViewController {
    fileprivate func addView() {
        self.view.addSubview(tableView)

        tableView.snp.makeConstraints { make in
            make.edges.equalTo(self.view.safeAreaLayoutGuide)
        }
    }
}
