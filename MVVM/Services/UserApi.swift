//
//  HomeViewController.swift
//  MVVM
//
//  Created by Aldina Galari on 26/12/22.
//

import Moya

public enum UserApi {
    case getUser(page: Int)
}

extension UserApi: TargetType {
    public var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }

    public var baseURL: URL {
        return URL(string: "https://randomuser.me/api/?page=1&results=10")!
    }

    public var path: String {
        switch self {
        case .getUser:
            return ""
        }
    }

    public var method: Moya.Method {
        switch self {
        case .getUser:
            return .get
        }
    }

    public var task: Task {
        switch self {
//        case .getUser(let page):
//            var params = [String: Any]()
//            params["page"] = page
//            params["results"] = 10
//            return .requestParameters(parameters: params,
//                                      encoding: URLEncoding.default)
        case .getUser:
            return .requestPlain
        }
    }

    public var sampleData: Data {
        return Data()
    }
}

public struct AppResponse<T: Decodable> {

    public let data: T?

    init(data: T?) {
        self.data = data
    }

    private enum CodingKeys: String, CodingKey {

        case data
    }
}

extension AppResponse: Decodable {

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do {
            data = try container.decode(T.self, forKey: .data)
        } catch {
            data = nil
        }
    }
}

public enum ResponseFormatter {

    case v1Object
    case v1Array

    func format<C: Decodable>(to: C.Type, from data: Data, statusCode: Int) -> Result<AppResponse<C>, NSError> {
        do {
            switch self {
            case .v1Object:
                return try formatV1(to: C.self, from: data, statusCode: statusCode)
            case .v1Array:
                return try formatV1(to: C.self, from: data, statusCode: statusCode)
            }
        } catch {
            return .failure(NSError(
                domain: "",
                code: statusCode,
                userInfo: [NSLocalizedDescriptionKey: error.localizedDescription]
            ))
        }
    }

    // swiftlint:disable line_length
    private func formatV1<C: Decodable>(to: C.Type, from data: Data, statusCode: Int) throws -> Result<AppResponse<C>, NSError> {
        let response = try JSONDecoder().decode(AppResponse<C>.self, from: data)
        if statusCode / 100 == 2 {
            return .success(response)
        }

        let error = NSError(
            domain: "",
            code: statusCode,
            userInfo: [NSLocalizedDescriptionKey: ""]
        )
        return .failure(error)
    }
}

// swiftlint:disable function_body_length
public extension Result where Success: Moya.Response {
    func map<C: Decodable>(to: C.Type, formatter: ResponseFormatter = .v1Object) -> Result<AppResponse<C>, NSError> {
        switch self {
        case .success(let moyaResponse):
            return formatter.format(to: C.self, from: moyaResponse.data, statusCode: moyaResponse.statusCode)
        case .failure(let error):
            return .failure(NSError(
                domain: "",
                code: 0,
                userInfo: [NSLocalizedDescriptionKey: error.localizedDescription]
            ))
        }
    }
}
